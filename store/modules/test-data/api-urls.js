const BASE_URL = '/v1/data-test'
const RESIDENT_URL = '/v1/resident'
const COMPLAIN_URL = '/v1/complain'
const COMPLAIN_COMMENT_URL = '/v1/complain-comments'
const APART_URL = '/v1/news'
const APART_COMMENT_URL = '/v1/news-comments'
const CATEGORY_URL = '/v1/meeting'
const ADMIN_URL = '/v1/manager'
const MEMBER_URL = '/v1/resident'

export default {
    DO_LIST: `${BASE_URL}/all`, //get
    DO_DETAIL: `${BASE_URL}/{id}`, //get
    DO_UPDATE: `${BASE_URL}/{id}`, //put
    DO_DELETE: `${BASE_URL}/{id}`, //del


  RESIDENT_LIST: `${RESIDENT_URL}/all`,//getList
  RESIDENT_DETAIL: `${RESIDENT_URL}/{id}`,

  COMPLAIN_LIST: `${COMPLAIN_URL}/list`,
  COMPLAIN_REGISTER: `${COMPLAIN_URL}/register`,
  COMPLAIN_PROGRESS: `${COMPLAIN_URL}/progress`,
  COMPLAIN_COMPLETION: `${COMPLAIN_URL}/completion`,
  COMPLAIN_DETAIL: `${COMPLAIN_URL}/complain/{complainId}`,

  COMPLAIN_COMMENT_LIST: `${COMPLAIN_COMMENT_URL}/list/{complainId}`,
  COMPLAIN_SET_COMMENT: `${COMPLAIN_COMMENT_URL}/comment/document-id/{id}`,

  APART_LIST: `${APART_URL}/newsList`,
  APART_SCHEDULE: `${APART_URL}/schedule`,
  APART_PROGRESS: `${APART_URL}/progress`,
  APART_COMPLETION: `${APART_URL}/completion`,
  APART_DETAIL: `${APART_URL}/news/{newsId}`,
  APART_SET: `${APART_URL}//new/resident`,

  APART_COMMENT_LIST: `${APART_COMMENT_URL}/list/{newsId}`,
  APART_SET_COMMENT: `${APART_COMMENT_URL}/resident/news/{id}`,

  CATEGORY_LIST: `${CATEGORY_URL}/list`,
  CATEGORY_BULLETIN: `${CATEGORY_URL}/list/getClimbing/{categoryValue}`,
  CATEGORY_DETAIL: `${CATEGORY_URL}/{meetingId}`,
  CATEGORY_SET: `${CATEGORY_URL}/new/meeting/{category}`,

  ADMIN_COAST: `${ADMIN_URL}/new/{id}`,

  MEMBER_SET: `${MEMBER_URL}/new/{residentGroup}`,


}
