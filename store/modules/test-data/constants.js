export default {
    DO_LIST: 'test-data/doList',
    DO_DETAIL: 'test-data/doDetail',
    DO_UPDATE: 'test-data/doUpdate',
    DO_DELETE: 'test-data/doDelete',


    RESIDENT_LIST: 'test-data/ResidentList',
    RESIDENT_DETAIL: 'test-data/ResidentDetail',

    COMPLAIN_LIST: 'test-data/ComplainList',
    COMPLAIN_REGISTER: 'test-data/ComplainRegister',
    COMPLAIN_PROGRESS: 'test-data/ComplainProgress',
    COMPLAIN_COMPLETION: 'test-data/ComplainCompletion',
    COMPLAIN_DETAIL: 'test-data/ComplainDetail',

    COMPLAIN_COMMENT_LIST: 'test-data/ComplainCommentList',
    COMPLAIN_SET_COMMENT: 'test-data/ComplainSetComment',

    APART_LIST: 'test-data/ApartList',
    APART_SCHEDULE: 'test-data/ApartSchedule',
    APART_PROGRESS: 'test-data/ApartProgress',
    APART_COMPLETION: 'test-data/ApartCompletion',
    APART_DETAIL: 'test-data/ApartDetail',
    APART_SET: 'test-data/ApartSet',

    APART_COMMENT_LIST: 'test-data/ApartCommentList',
    APART_SET_COMMENT: 'test-data/APartSetComment',

    CATEGORY_LIST: 'test-data/CategoryList',
    CATEGORY_BULLETIN: 'test-data/CategoryBulletin',
    CATEGORY_DETAIL: 'test-data/CategoryDetail',
    CATEGORY_SET: 'test/data/CategorySet',

    ADMIN_COAST: 'test-data/AdminCoast',

    MEMBER_SET: 'test-data/MemberSet',




}
