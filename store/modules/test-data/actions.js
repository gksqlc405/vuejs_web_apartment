import axios from 'axios'
import apiUrls from './api-urls'
import Constants from './constants'

export default {
    [Constants.DO_LIST]: (store) => {
        // 백이 페이징되어있을 때 : 예. /all/{pageNum}
        // return axios.get(apiUrls.DO_LIST.replace('{pageNum}', payload.pageNum))
        return axios.get(apiUrls.DO_LIST)
    },
    [Constants.DO_DETAIL]: (store, payload) => {
        return axios.get(apiUrls.DO_DETAIL.replace('{id}', payload.id))
    },
    [Constants.DO_UPDATE]: (store, payload) => {
        return axios.get(apiUrls.DO_UPDATE.replace('{id}', payload.id))
    },
    [Constants.DO_DELETE]: (store, payload) => {
        return axios.get(apiUrls.DO_DELETE.replace('{id}', payload.id))
    },


  [Constants.RESIDENT_LIST]: (store) => {
    return axios.get(apiUrls.RESIDENT_LIST)
  },

  [Constants.RESIDENT_DETAIL]: (store, payload) => {
    return axios.get(apiUrls.RESIDENT_DETAIL.replace('{id}', payload.id))
  },

  [Constants.COMPLAIN_LIST]: (store) => {
    return axios.get(apiUrls.COMPLAIN_LIST)
  },

  [Constants.COMPLAIN_REGISTER]: (store) => {
    return axios.get(apiUrls.COMPLAIN_REGISTER)
  },

  [Constants.COMPLAIN_PROGRESS]: (store) => {
    return axios.get(apiUrls.COMPLAIN_PROGRESS)
  },

  [Constants.COMPLAIN_COMPLETION]: (store) => {
    return axios.get(apiUrls.COMPLAIN_COMPLETION)
  },

  [Constants.COMPLAIN_DETAIL]: (store, payload) => {
    return axios.get(apiUrls.COMPLAIN_DETAIL.replace('{complainId}', payload.complainId))
  },

  [Constants.COMPLAIN_COMMENT_LIST]: (store, payload) => {
    return axios.get(apiUrls.COMPLAIN_COMMENT_LIST.replace('{complainId}', payload.complainId))
  },

  [Constants.COMPLAIN_SET_COMMENT]: (store, payload) => {
    return axios.post(apiUrls.COMPLAIN_SET_COMMENT.replace('{id}',payload.id),payload.data)

  },

  [Constants.APART_LIST]: (store) => {
    return axios.get(apiUrls.APART_LIST)
  },

  [Constants.APART_SCHEDULE]: (store) => {
    return axios.get(apiUrls.APART_SCHEDULE)
  },

  [Constants.APART_PROGRESS]: (store) => {
    return axios.get(apiUrls.APART_PROGRESS)
  },

  [Constants.APART_COMPLETION]: (store) => {
    return axios.get(apiUrls.APART_COMPLETION)
  },
  [Constants.APART_DETAIL]: (store, payload) => {
    return axios.get(apiUrls.APART_DETAIL.replace('{newsId}', payload.newsId))
  },

  [Constants.APART_COMMENT_LIST]: (store, payload) => {
    return axios.get(apiUrls.APART_COMMENT_LIST.replace('{newsId}', payload.newsId))
  },

  [Constants.APART_SET]: (store, payload) => {
    return axios.post(apiUrls.APART_SET, payload)
  },

  [Constants.APART_SET_COMMENT]: (store, payload) => {
    return axios.post(apiUrls.APART_SET_COMMENT.replace('{id}',payload.id),payload.data)

  },


  [Constants.CATEGORY_LIST]: (store) => {
    return axios.get(apiUrls.CATEGORY_LIST)
  },

  [Constants.CATEGORY_BULLETIN]: (store, payload) => {
    return axios.get(apiUrls.CATEGORY_BULLETIN.replace('{categoryValue}', payload.categoryValue))
  },

  [Constants.CATEGORY_DETAIL]: (store, payload) => {
    return axios.get(apiUrls.CATEGORY_DETAIL.replace('{meetingId}', payload.meetingId))
  },

  [Constants.CATEGORY_SET]: (store, payload) => {
    return axios.post(apiUrls.CATEGORY_SET.replace('{category}',payload.category),payload.data)
  },


  [Constants.ADMIN_COAST]: (store, payload) => {
    return axios.post(apiUrls.ADMIN_COAST.replace('{id}',payload.id),payload.data)

  },

  [Constants.MEMBER_SET]: (store, payload) => {
    return axios.post(apiUrls.MEMBER_SET.replace('{residentGroup}',payload.residentGroup),payload.data)
  },

}
