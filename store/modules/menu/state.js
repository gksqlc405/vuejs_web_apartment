const state = () => ({
    globalMenu: [
        {
            parentName: 'HOME',
            menuLabel: [
                { id: 'DASH_BOARD', icon: 'el-icon-tickets', currentName: '대시보드', link: '/', isShow: true },
            ]
        },
        {
            parentName: '입주자관리',
            menuLabel: [
                { id: 'ADMIN_ADD', icon: 'el-icon-tickets', currentName: '관리비 등록', link: '/', isShow: false },
                { id: 'MEMBER_LIST', icon: 'el-icon-tickets', currentName: '입주자 리스트', link: '/customer/list', isShow: true },
                { id: 'MEMBER_DETAIL', icon: 'el-icon-tickets', currentName: '회원상세정보', link: '/', isShow: false },
                { id: 'MEMBER_ADD', icon: 'el-icon-tickets', currentName: '입주자 등록', link: '/customer/member-form', isShow: true },

            ]
        },

      {
        parentName: '민원창구',
        menuLabel: [
          { id: 'COMPLAIN_LIST', icon: 'el-icon-tickets', currentName: '민원창구 게시글  ', link: '/customer/complain-list', isShow: true },
          { id: 'COMPLAIN_DETAIL', icon: 'el-icon-tickets', currentName: '민원창구 게시글 상세페이지', link: '/', isShow: false },
        ]
      },

      {
        parentName: '아파트소식',
        menuLabel: [
          { id: 'APART_LIST', icon: 'el-icon-tickets', currentName: '아파트소식 게시글', link: '/customer/apt-list', isShow: true },
          { id: 'APART_DETAIL', icon: 'el-icon-tickets', currentName: '아파트소식 게시글 상세페이지', link: '/', isShow: false },
          { id: 'APART_ADD', icon: 'el-icon-tickets', currentName: '아파트소식 게시글 등록', link: '/', isShow: false },
        ]
      },

      {
        parentName: '입주민 라운지',
        menuLabel: [
          { id: 'CATEGORY_LIST', icon: 'el-icon-tickets', currentName: '카테고리', link: '/customer/category-list', isShow: true },
          { id: 'CATEGORY_BULLETIN', icon: 'el-icon-tickets', currentName: '카테고리 게시글 ', link: 'customer/category-bulletin', isShow: false },
          { id: 'CATEGORY_DETAIL', icon: 'el-icon-tickets', currentName: '카테고리 게시글 상세페이지 ', link: '/', isShow: false },
          { id: 'CATEGORY_SET', icon: 'el-icon-tickets', currentName: '카테고리 게시글 등록 ', link: 'customer/category-form', isShow: false },
        ]
      },

        {
            parentName: '관리자 메뉴',
            menuLabel: [
                { id: 'MEMBER_LOGOUT', icon: 'el-icon-lock', currentName: '로그아웃', link: '/my-menu/logout', isShow: true },
            ]
        },
    ],
    selectedMenu: 'DASH_BOARD'
})

export default state
